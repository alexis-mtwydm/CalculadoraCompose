package com.example.calculadoracompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.calculadoracompose.ui.theme.CalculadoraComposeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CalculadoraComposeTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Calculadora()
                }
            }
        }
    }
}

@Composable
fun Actionbutton(action:String, color:Color){
    Button(
        onClick = { /*TODO*/ },
        modifier = Modifier
            .padding(5.dp)
            .height(140.dp)
            .width(75.dp),
        colors = ButtonDefaults.buttonColors(backgroundColor = color),
        shape = CircleShape
    ) {
        Text(text = action, color = Color.White)
    }
}

@Composable
fun ButtonNumber(number:String, color:Color){
    Button(
        onClick = { /*TODO*/ },
        modifier = Modifier
            .padding(1.dp)
            .height(70.dp)
            .width(67.dp),
        colors = ButtonDefaults.buttonColors(backgroundColor = color),
        shape = CircleShape
    ) {
        Text(text = number, color = Color.White)
    }
}

@Composable
fun Calculadora() {
    Scaffold(backgroundColor = Color.Black) {
        Text(
            text = "0",
            modifier = Modifier.fillMaxWidth(),
            textAlign = TextAlign.Right,
            fontSize = 80.sp,
            color = Color.White)

        Row(
            Modifier.fillMaxHeight(),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.Bottom
        ) {


            Column(Modifier.padding(3.dp)) {
                ButtonNumber("7", Color(0xFF373b38))
                ButtonNumber("4", Color(0xFF373b38))
                ButtonNumber("1", Color(0xFF373b38))
                ButtonNumber("AC",Color(0xFF373b38))
            }
            Column(Modifier.padding(3.dp)) {
                ButtonNumber("8", Color(0xFF373b38))
                ButtonNumber("5", Color(0xFF373b38))
                ButtonNumber("2", Color(0xFF373b38))
                ButtonNumber("0", Color(0xFF373b38))

            }
            Column(Modifier.padding(3.dp)) {
                ButtonNumber("9", Color(0xFF373b38))
                ButtonNumber("6", Color(0xFF373b38))
                ButtonNumber("3", Color(0xFF373b38))
                ButtonNumber(".", Color(0xFF373b38))
            }
            Column(Modifier.padding(3.dp)) {
                ButtonNumber("÷", Color(0xFF373b38))
                ButtonNumber("x", Color(0xFF373b38))
                ButtonNumber("-", Color(0xFF373b38))
                ButtonNumber("+", Color(0xFF373b38))
                //Color(0xFF316ec4)
            }
            Column(
                Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.Start
            ) {
                Actionbutton("⌫", Color(0xFF316ec4))
                Actionbutton("=", Color(0xFF316ec4))
            }
        }

    }

}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    CalculadoraComposeTheme {
        Calculadora()
    }
}